# Project 2 - Bomberman (UET OASIS - I2122 INT2204 24 - Object-oriented Programming)

## Contributors
    + Bùi Thùy Dương (20021320) - K65CACLC2
    + Nguyễn Minh Đức (20021337) - K65CACLC3
    + Nguyễn Diệu Quỳnh (20021424) - K65TCLC

## Demo
   <img src="res/demo.png" alt="drawing" width="800"/>

## UML
   <img src="res/BombermanUML.png" alt="drawing" width="800"/>

## Features
- Gói bắt buộc (+8đ)
1. Thiết kế cây thừa kế cho các đối tượng game **+2đ**
2. Xây dựng bản đồ màn chơi từ tệp cấu hình (có mẫu tệp cấu hình) **+1đ**
3. Di chuyển Bomber theo sự điều khiển từ người chơi **+1đ**
4. Tự động di chuyển các Enemy **+1đ**
5. Xử lý va chạm cho các đối tượng Bomber, Enemy, Wall, Brick, Bomb **+1đ**
6. Xử lý bom nổ **+1đ**
7. Xử lý khi Bomber sử dụng các Item và khi đi vào vị trí Portal **+1đ**

- Gói tùy chọn
1. + Nâng cấp thuật toán tìm đường cho Enemy (bfs) **+0.5đ**
   + Cài đặt thêm các loại Enemy khác: **+0.25đ** cho mỗi loại enemy (cài đặt thêm **3** loại Enemy: Doll, Pontan, Kondoria) 
2. Xử lý hiệu ứng âm thanh (thêm music & sound effects) **+1đ**
3. Những ý tưởng khác sẽ được đánh giá và cộng điểm theo mức tương ứng
   + Thêm menu, chuyển màn
   + Thêm thời gian chơi, tính điểm, tính mạng
